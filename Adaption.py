from pattern.db import Datasheet
import math
import os
class Adaption:

	def __init__(self, loc=None):
		if loc is not None and os.path.exists(loc):
			self.loc = loc
			self.datasheet = Datasheet.load(loc)
		else:
			self.loc = "data/adaption_data.csv"
			self.datasheet = Datasheet()
			
	def __del__(self):
		self.datasheet.save(self.loc)

			
	def add_row(self, row):
		rr = row + [0,0]
		self.datasheet.append(rr)
		
	def update_all_rows(self, twitter):
		for i in range(len(self.datasheet)):
			fav, ret = self.get_status_performance(twitter, self.datasheet[i][0])
			self.datasheet[i][-2] = ret
			self.datasheet[i][-1] = fav
			
	def update_row_retweets(self, post_id, retweet_count=-1, favorite_count=-1):
		for i in range(len(self.datasheet)):
			if self.datasheet[i][0] == post_id:
				if retweet_count > 0:
					self.datasheet[i][-2] = retweet_count
				if favorite_count > 0:
					self.datasheet[i][-1] = favorite_count
					
	def get_status_performance(self, twitter, tweet_id):
		perf = twitter.get_status(tweet_id)
		favorite = perf['favorite_count']
		retweets = perf['retweeted']
		return favorite, retweets
		
	def euclidean(self, a, b):
		dists = 0.0
		for i in range(len(a)):
			dists += (a[i]-b[i])*(a[i]-b[i])
		return math.sqrt(dists)
		
	def expected_actions(self, l):
		favs = list()
		ret = list()
		for i in l:
			favs.append(float(i[-1]))
			ret.append(float(i[-2]))
		if len(favs) > 0:
			fav_ex = sum(favs)/len(favs)
		else:
			fav_ex = 0
		if len(ret) > 0:
			ret_ex = sum(ret)/len(ret)
		else:
			ret_ex = 0
		return 0.5*fav_ex + 0.5*ret_ex
		
	def score(self, row):
		best_matches = self.knn(row)
		scr = self.expected_actions(best_matches)
		return scr
		
	def knn(self, row, k=5):
		distance = list()
		for r in range(len(self.datasheet)):
			distance.append([r, self.euclidean(self.datasheet[r][3:], row)])
		distance = sorted(distance, key=lambda x: x[1])
		distance = distance[:k]
		return distance