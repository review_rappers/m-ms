# metacritic API - cnXXUM1q9amshRtLP81cy6Rlgt9wp1hc5Mhjsnp5z37WfCpFM2

import unirest
from pattern.db import Datasheet
import os
from pattern.web import download
import re
from HTMLParser import HTMLParser
from rottentomatoes import RT
import time
import random
import nltk

def download_rotten_reviews(movie):
	rt = RT("b5dvcs9sz72xy6h5sj9xwrdw").search(movie)
	try:
		rt_link = rt[0]["links"]["alternate"]
	except:
		return []
	i = 0
	res = list()
	while True:
		i += 1
		html = download(rt_link + "/reviews/?page=" + str(i) + "&type=user", unicode=True)
		parser = HTMLParser()
		html = parser.unescape(html)
		results = re.findall('<a class="bold unstyled articleLink" href="/user/id/[0-9]+?/" >(.+?)<div class="scoreWrapper">(.+?)<td>', html, re.DOTALL)
		if len(results) == 0:
			break
		for r in results:
			score = len(re.findall("glyphicon glyphicon-star", r[0]))
			review = re.findall("</div>(.+?)</div>", r[1], re.DOTALL)[0]
			review = review.replace("\n", " ")
			review = re.sub("<.+?>", "", review)
			review = review.strip()
			res.append([score, review, "<sent_sep>".join(nltk.sent_tokenize(review))])
		time.sleep(random.randint(1,5))
	return res

def get_reviews(movie_name):
	res = list()
	key = "cnXXUM1q9amshRtLP81cy6Rlgt9wp1hc5Mhjsnp5z37WfCpFM2"
	response = unirest.post("https://byroredux-metacritic.p.mashape.com/find/movie",
	  headers={
		"X-Mashape-Key": key,
		"Content-Type": "application/x-www-form-urlencoded",
		"Accept": "application/json"
	  },
	  params={
		"retry": 4,
		"title": movie_name
	  }
	)
	
	result = response.body["result"]
	if result != False:
		response = unirest.get("https://byroredux-metacritic.p.mashape.com/user-reviews?page_count=all&url=" + result["url"],
		  headers={
			"X-Mashape-Key": key,
			"Accept": "application/json"
		  }
		)
	
		reviews = response.body["reviews"]
		for r in reviews:
			score = r["score"]
			review = r["review"]
			res.append([score, review, "<sent_sep>".join(nltk.sent_tokenize(review))])
	return res	
	
def save_reviews(tbl, movie):
	datasheet = Datasheet()
	for r in tbl:
		datasheet.append(r)
	datasheet.save("reviews/" + movie + ".csv")
	
def load_movie_list():
	f = open("data/imdb_top250_movies.txt")
	dat = map(lambda x: x.strip(), f.read().split("\n"))
	f.close()
	return dat

movies = load_movie_list()
for m in movies:
	if not os.path.exists("reviews/" + m + "_rt.csv"):
		resu = download_rotten_reviews(m)
		save_reviews(resu, m + "_rt")
		print m, len(resu)
		s = False
	else:
		print "Skipped",m
		s = False

for m in movies:
	s = True
	while s:
		try:
			if not os.path.exists("reviews/" + m + "_meta.csv"):
				reviews = get_reviews(m)
				if len(reviews) > 0:
					save_reviews(reviews, m + "_meta")
				print m, len(reviews)
				s = False
			else:
				print "Skipped:",m
				s = False
		except:
			continue
		
