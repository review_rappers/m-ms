import json
from pattern.web import URL, DOM, plaintext
from pattern.en import wordnet, tag, NOUN, VERB, ADJECTIVE, ADVERB
import re

class RhymeBrain:
    baseurl = 'http://rhymebrain.com/talk?function=getRhymes&word='

    def __init__(self, word):
        if not word:
            return
        self.word = word
        url = URL(RhymeBrain.baseurl + word)
        json_data = url.download(cached=True, throttle=2)
        data = json.loads(json_data)

        self.rhymes = data
        self.words_rhymes = [x['word'] for x in data]
        #print self.words_rhymes

class Rhyme:

    @staticmethod
    def get_syn_last_word(sentence):
        synonyms = []
        last_tag = tag(sentence)[-1]
        w1 = last_tag[0]
        pos1 = last_tag[1]
        if pos1 == 'NN':
            s1 = wordnet.synsets(w1, pos=NOUN)
            if s1:
                s1_syn = s1[0].synonyms
                synonyms.extend(s1_syn)
        elif pos1 == 'JJ':
            s1 = wordnet.synsets(w1, pos=ADJECTIVE)
            if s1:
                s1_syn = s1[0].synonyms
                synonyms.extend(s1_syn)
        elif pos1 == 'RB':
            s1 = wordnet.synsets(w1, pos=ADVERB)
            if s1:
                s1_syn = s1[0].synonyms
                synonyms.extend(s1_syn)
        return synonyms

    @staticmethod
    def rhyme(sentence, list_sentences):
        if not sentence or not list_sentences:
            return None

        sentences_rhymes = []
        #get words which rhyme with last of sentence
        last_word = sentence.split(' ')[-1]

        rhymes = RhymeBrain(last_word)
        synonyms = Rhyme.get_syn_last_word(sentence)
        synonyms = list(set(synonyms).intersection(rhymes.words_rhymes))

        rw_ws = {}#rhyme word wordnet synsets

        for word in rhymes.words_rhymes:
            try:
                rw_ws[word]= wordnet.synsets(word)
            except:
                pass

        for s in list_sentences:
            lw_sentence = s.split(' ')[-1]
            if lw_sentence in rhymes.words_rhymes:
                sentences_rhymes.append(s)
            else:
                for synonym_rhyme in synonyms:
                    if lw_sentence.lower().endswith(synonym_rhyme.lower()):
                        ns = s.replace(lw_sentence, synonym_rhyme)
                        sentences_rhymes.append(ns)
            # else: #if they don't rhyme, try to force it?
            #     ws = wordnet.synsets(lw_sentence)
            #     if ws:
            #         ws = ws[0]
            #         for word in rw_ws:
            #             for wwsi in rw_ws[word]:
            #                 try:
            #                     #if ws is synenoms of wwsi
            #                     if wordnet.similarity(ws, wwsi) > 0.5 \
            #                         and wwsi.synonyms[0].lower() not in lw_sentence.lower():
            #                         ns = s.replace(lw_sentence, wwsi.synonyms[0])
            #                         sentences_rhymes.append(ns)
            #                 except:
            #                     pass

        return sentences_rhymes



def replace_synynoms(sentence):
    sentences = []
    syn = Rhyme.get_syn_last_word(sentence)
    last_word = sentence.split(' ')[-1]
    for s in syn:
        snew = sentence.replace(last_word, s)
        sentences.append(snew)

    return sentences

#for sentence in replace_synynoms("I love driving my car"):
#    print sentence
#    print Rhyme.rhyme(sentence, ["All this books are in a pile"])