import os
import nltk
from pattern.db import Datasheet
import pickle 
import metre_rhyme as mr
from nltk.tokenize import RegexpTokenizer
from pattern.en import sentiment

class Review:

	def __init__(self, movie, rating, review, review_splitted):
		self.movie = movie
		self.rating = rating
		self.review = review
		tokenizer = RegexpTokenizer(r'\w+')
		self.review_splitted = []
		for r in review_splitted:
			s = r
			ls = len(s)
			nss = mr.n_syllables(s)
			r = r.replace("'", "")
			tk = tokenizer.tokenize(r)
			ltk = len(tk)
			tk = " ".join(tk)
			self.review_splitted.append([s, ls, nss, tk, ltk])

class Reviews:

	def __init__(self, movie):
		#self.movies = self.load_movie_list()
		self.reviews = list()
		self.get_movie_reviews(movie)
		
	def load_movie_list(self):
		f = open("data/imdb_top250_movies.txt")
		dat = map(lambda x: x.strip(), f.read().split("\n"))
		f.close()
		return dat
		
	def load_rotten(self, movie):
		if os.path.exists("reviews/" + movie + "_rt.csv"):
			datasheet = Datasheet.load("reviews/" + movie + "_rt.csv")
			for d in datasheet:
				self.reviews.append(Review(movie, int(d[0])*2, d[1], d[2].split("<sent_sep>")))
			
	def load_metacritic(self, movie):
		if os.path.exists("reviews/" + movie + "_meta.csv"):
			datasheet = Datasheet.load("reviews/" + movie + "_meta.csv")
			for d in datasheet:
				self.reviews.append(Review(movie, int(d[0]), d[1], d[2].split("<sent_sep>")))
		
	def get_movie_reviews(self, movie):
		self.load_rotten(movie)
		self.load_metacritic(movie)
		
	def get_negative_reviews(self, score=3):
		ret = list()
		for r in self.reviews:
			if r.rating < score and sentiment(r.review)[0] < 0:
				ret.append(r)
		return ret
		
	def get_positive_reviews(self, score=7):
		ret = list()
		for r in self.reviews:
			if r.rating > score and sentiment(r.review)[0] > 0:
				ret.append(r)
		return ret


### For testing
if __name__ == "__main__":
	r = Reviews()
	r.get_movie_reviews("Gremlins")
	for rr in r.reviews:
		print rr.review_splitted
		print rr.movie
		print rr.rating
		exit()