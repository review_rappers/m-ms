from phonemes import *
from pattern.en import parsetree, tokenize
import re

rhyme_weight = 3
syl_weight = 1

#def n_syllables(text):
#	n = 0
#	for sentence in parsetree(text):
#		for chunk in sentence.chunks:
#			for word in chunk.words:
#				n += syllables(word.string)
#	return n
	
def n_syllables(text):
	n = 0
	for word in tokenize(text)[0].split():
#		print word
		if re.match("\w+", word):
			nw = syllables(word)
			if nw:
				n += nw
			else:
				n += 1
	return n

def text_rhymes(t1, t2):
	w1 = last_word(t1)
	w2 = last_word(t2)
	#3 matching ending phonemes
	if w2.upper() in rhymes_for_word(w1, 2):
		return True
	return False

def last_word(text):
	p = tokenize(text)[0].split()
	return p[-1]
	
def matching_score(t1, t2):
	score = -abs(n_syllables(t1) - n_syllables(t2))
	if text_rhymes(t1, t2):
		score += rhyme_weight
	return score
	
def matching_score_nsyllables(t1, t2, n1, n2):
	score = -abs(n1 - n2)
	if text_rhymes(t1, t2):
		score += rhyme_weight
	return score


### For testing
if __name__ == "__main__":
	print n_syllables("giraffe")
	print n_syllables("bro")

	#s1 = "So the FCC won't let me be or let me be me so let me see"
	#s2 = "they tried to shut me down on MTV but it feels so empty without me"

	s1 = "Middle of the night and yes"
	s2 = "Slow server"

	print n_syllables(s1)
	print n_syllables(s2)

#	print last_word(s1)
#	print last_word(s2)
	print text_rhymes(s1, s2)
	print matching_score(s1, s2)


	
	
	