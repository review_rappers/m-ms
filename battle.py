from Reviews import *
from Character import *
import similarity as s
import noc
import random
import glob


# Choose opponents

a = list(noc.assoc(noc.xlsx("NOC_List/NOC_List.xlsx")))

opponent = []
while not opponent:
	n = random.randint(0,len(a))
	char_a = Character(a[n])
	if char_a.num_quotes:
		opponent = s.find_opponent(char_a.me)

		if opponent:
			char_b = Character(opponent[0][0])
			if not char_b.num_quotes:
				opponent = None
	

print char_a.name,"vs",char_b.name

### Assign sides
r = random.random()

a_side = -1

if r > 0.5:
	a_side = 1

movies = glob.glob("reviews/*.csv")
random.shuffle(movies)
movie = movies[0].split("/")[1].split("_")[0]

r = Reviews(movie)

pos = r.get_positive_reviews()
neg = r.get_negative_reviews()

needed_tweets = 5

if a_side == -1:
	char_a.set_reviews(neg)
	char_b.set_reviews(pos)
else:
	char_a.set_reviews(pos)
	char_b.set_reviews(neg)

char_a.rap(needed_tweets)	
char_b.rap(needed_tweets)

tweets = [ [ "Legendary MCs " + char_a.name + " (MC " + char_a.initials + ") vs " + char_b.name + " (MC " + char_b.initials + ") reviewing " + movie + " #codecampcc", [] ] ]

previous_lines = list()
for i in range(needed_tweets):
	nexta = char_a.get_next_line(previous_lines)
	tweets.append([ char_a.initials + ":\n" + "\n".join(nexta[:2]), nexta[2:] ])
	previous_lines.append(nexta[:2])
	nextb = char_b.get_next_line(previous_lines)
	previous_lines.append(nextb[:2])
	tweets.append([ char_b.initials + ":\n" + "\n".join(nextb[:2]), nextb[2:] ])
	
for t in tweets:
	print t[0]
	#time.sleep(121)