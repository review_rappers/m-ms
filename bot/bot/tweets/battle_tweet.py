import __builtin__
import json
import random
import collections
import bot
from tweet import Tweet
from pattern.en import singularize
import utils
import database
from pattern.en.wordlist import BASIC
from pattern.web import DuckDuckGo
import services

from pattern.db import Datasheet, INTEGER, STRING
from pattern.db import uid, pprint
ds = Datasheet(rows=[], fields=[
    ("id", INTEGER),  # Define the column headers.
    ("Character", STRING),
    ("quotes", STRING),
    ("keywords", STRING),
    ("genres", STRING)
])

class BattleTweet(Tweet):
    _patterns = []

    def __init__(self):
        pass

    def generate(self):
        character_x = random.choice(utils.NocUtil.noc())#get random character
        # dgg = DuckDuckGo(language='en')
        # print dgg.search("DuckDuckGo")


        for row in utils.NocUtil.noc():
            character = row["Character"]
            try:
                imdb = services.Imdb(character)
                ds.rows.append([
                    uid(),
                    character,
                    json.dumps(imdb.quotes),
                    json.dumps(imdb.keywords),
                    json.dumps(imdb.genres),
                ])
                print character
            except:
                ds.rows.append([
                    uid(),
                    character,
                    "",
                    "",
                    "",
                ])

        ds.save("data/imdb.txt", headers=True)


        # wq = services.WikiQuote("batman")
        # print wq.quotes
        tweet = ''
        return tweet