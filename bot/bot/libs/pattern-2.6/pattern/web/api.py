# --- API LICENSE CONFIGURATION -----------------------------------------------------------------------
# Default license keys used by pattern.web.SearchEngine to contact different API's.
# Google and Yahoo are paid services for which you need a personal license + payment method.
# The default Google license is for testing purposes (= 100 daily queries).
# Wikipedia, Twitter and Facebook are free.
# Bing, Flickr and ProductsWiki use licenses shared among all Pattern users.

license = {}
license["Google"] = \
    ""

license["Bing"] = \
    ""

license["Yahoo"] = \
    ("", "")  # OAuth (key, secret)

license["DuckDuckGo"] = \
    None

license["Wikipedia"] = \
    None

license["Twitter"] = (
    "",  # OAuth (key, secret, token)
    "", (
        "",
        ""))

license["Facebook"] = \
    "332061826907464|jdHvL3lslFvN-s_sphK1ypCwNaY"

license["Flickr"] = \
    "787081027f43b0412ba41142d4540480"

license["ProductWiki"] = \
    "64819965ec784395a494a0d7ed0def32"

