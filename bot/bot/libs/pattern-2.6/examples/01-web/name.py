import os
import sys

from pattern.vector import SVM, chngrams, count

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", ".."))

from pattern.db import pd


class GenderByName(SVM):
    def train(self, name, gender=None):
        SVM.train(self, self.vector(name), gender)

    def classify(self, name):
        return SVM.classify(self, self.vector(name))

    def vector(self, name):
        """ Returns a dictionary with character bigrams and suffix.
            For example, "Felix" => {"Fe":1, "el":1, "li":1, "ix":1, "ix$":1, 5:1}
        """
        v = chngrams(name, n=2)
        v = count(v)
        v[name[-2:] + "$"] = 1
        v[len(name)] = 1
        return v


g = GenderByName.load(pd("gender-by-name.svm"))

for name in (
        "Felix",
        "Felicia",
        "Rover",
        "Kitty",
        "Legolas",
        "Arwen",
        "Jabba",
        "Leia",
        "Flash",
        "Barbarella"):
    print name, g.classify(name)

# In the example above, Arwen and Jabba are misclassified.
# We can of course improve the classifier by hand:

# g.train("Arwen", gender="f")
#g.train("Jabba", gender="m")
#g.save(pd("gender-by-name.svm"), final=True)
#print g.classify("Arwen")
#print g.classify("Jabba")
