import re


class StringUtil:
    @staticmethod
    def find_hex(text):
        regex = re.compile("[#|0x]([A-Fa-f0-9]{6})")
        return regex.findall(text)

    @staticmethod
    def find_url(text):
        regex = re.compile("\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]")
        return regex.findall(text)

    @staticmethod
    def remove_url(text):
        regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]"
        return re.sub(regex, '', text)

    @staticmethod
    def find_num(text):
        regex=r'[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?'
        return re.findall(regex, text)

    @staticmethod
    def sentences(text):
        regex = re.compile("([ a-zA-Z]+)", re.IGNORECASE)
        return regex.findall(text)