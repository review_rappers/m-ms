import re
import json

import tweepy
from pattern.web import URL
from pattern.text import language
from pattern.web import Twitter

import bot
import utils
from pattern.en import positive, wordnet
from pattern.en import pluralize, singularize
import services


class TwitterUtil:

    _hashtag_pattern = "[#]([A-Za-z0-9_]+)"
    _mention_pattern = "[@]([A-Za-z0-9_]+)"

    @staticmethod
    def find_hashtags(text):
        regex = re.compile(TwitterUtil._hashtag_pattern)
        return regex.findall(text)

    @staticmethod
    def remove_hashtags(text):
        return re.sub(TwitterUtil._hashtag_pattern, '', text)


    @staticmethod
    def find_mentions(text):
        regex = re.compile(TwitterUtil._mention_pattern)
        return regex.findall(text)

    @staticmethod
    def remove_mentions(text):
        return re.sub(TwitterUtil._mention_pattern, '', text)

    @staticmethod
    def remove_rt(text):
        if text.startswith('RT '):
            text = text.replace('RT ', '', 1)
        text = text.replace(' RT ', '')
        text = text.replace(' rt ', '')
        return text


    @staticmethod
    def clean_tweet(text):
        text = TwitterUtil.remove_rt(text) #remote RT
        text = TwitterUtil.remove_hashtags(text) #remove #...
        text = TwitterUtil.remove_mentions(text) #remove @...
        text = utils.StringUtil.remove_url(text) #remove http:///...
        text = text.strip(' \t\n\r') #strip
        return text

    #a function that predicts in which category the user fits in
    @staticmethod
    def account_category(user):

        nouns = []
        categories = []


        name = user['name']
        screen_name = user['screen_name']

        #twitter description
        description = user['description']
        clean_description = TwitterUtil.clean_tweet(description)

        #find all nouns in sentence
        nouns = nouns + utils.NlpUtil.find_nouns(clean_description)

        '''
        #in latest tweets, find buy|purchase NOUN
        tweets = bot.twitter.user_timeline(user_id=user['id_str'], count=1000)
        for status in tweets:
            tweet = status['text']
            clean_tweet = TwitterUtil.clean_tweet(tweet)
            clean_tweet_l = clean_tweet.lower()
            if 'buy' in clean_tweet_l or 'purchase' in clean_tweet_l:
                nouns = nouns + utils.NlpUtil.find_nouns(clean_tweet)

        nouns = [noun.lower() for noun in nouns] #convert them all to lower
        most_nouns = Counter(nouns).most_common(5) #get most 3 common nouns

        for noun, it in most_nouns:
            if wordnet.synsets(singularize(noun)) or wordnet.synsets(pluralize(noun)):
                continue

            cats = TwitterUtil.find_category(noun) #find category
            if cats:
                categories += cats
        '''

        cats = TwitterUtil.find_category(name) #find category
        if cats:
                categories += cats
        if name.lower() != screen_name.lower():
            cats = TwitterUtil.find_category(screen_name) #find category
            if cats:
                categories += cats

        return categories

    @staticmethod
    def find_category(noun):
        #make sure it is not english word
        word_wn = wordnet.synsets(noun, pos=wordnet.NOUN)
        if word_wn:
            return None

        categories = [] #array containing all categories possible

        #wikipedia (first sentence + category)
        wiki_data_url = URL("https://en.wikipedia.org/w/api.php?format=json&action=query&titles=" + noun.capitalize() + "&prop=extracts|categories|pageprops|revisions&exintro=&continue&rvprop=content&exsentences=1&explaintext&redirects")
        wiki_data = json.loads(wiki_data_url.download())
        try:
            for page_id in wiki_data['query']['pages']:
                page = wiki_data['query']['pages'][page_id]

                title = page['title']
                if 'extract' not in page:
                    continue

                description = page['extract']  # find first sentence from description
                description_category = utils.NlpUtil.find_is_a_noun(description)
                categories.append(description_category)  #add to categories array

                # for category in page['categories']:
                #    category_title = category['title'][9:]#remove Category:
                #categories.append( category_title ) #add to categories array


                # in side infobox
                #interested in
                # first word
                # type
                # genre..
                # family
                revesion = page['revisions'][0]['*']
                infobox_spos = revesion.find('{{infobox')
                infobox_spos = infobox_spos if infobox_spos != -1 else revesion.find('{{Infobox')
                if infobox_spos != -1:
                    infobox_epos = revesion.find('}}\n\n', infobox_spos)
                    if infobox_epos != -1:
                        infobox_data = revesion[infobox_spos + 10:infobox_epos -1]

                        infobox_data = infobox_data.split("|")
                        if len(infobox_data) > 0:
                            wiki_type = infobox_data[0].strip(' \t\n\r')  # strip
                            categories.append(wiki_type)  # add to categories array
                            for info in infobox_data[1:]:
                                eq_pos = info.find('=')
                                property = info[0:eq_pos].strip(' \t\n\r').lower()
                                data = info[eq_pos + 1:].strip(' \t\n\r').lower()

                                if property.startswith('type') or \
                                        property.startswith('genre') or \
                                        property.startswith('family'):
                                    data = data.replace('[[', '').replace(']]', '').split(
                                        '<br />')  # make it list of values
                                    categories.append(data[0].strip(' \t\n\r'))  # add to categories array
        except:
            pass

        #freebase
        try:
            sing_noun = singularize(noun)
            freebase = services.Freebase()
            freebase.search_term(sing_noun)

            #print freebase.types
            if '/common/topic/description' in freebase.properties:
                freebase_description_category = utils.NlpUtil.find_is_a_noun(freebase.properties['/common/topic/description'])
                categories.append( freebase_description_category ) #add to categories array

            if '/common/topic/alias' in freebase.properties:
                known_as = freebase.properties['/common/topic/alias']
                categories.append( known_as ) #add to categories array
        except:
            pass


        return categories


    @staticmethod
    def get_friends(user_id, verified=False, pages=1):
        ids = []
        friends = []
        for page in tweepy.Cursor(bot.twitter.friends_ids, id=user_id, wait_on_rate_limit=True).pages(pages):
            # process status here
            ids.extend(page['ids'])
            chunks = list(utils.ContainerUtil.chunks(page['ids'], 100))
            for chunk in chunks:
                users = bot.twitter.lookup_users(chunk)
                for user in users:
                    if not verified or \
                        (verified and user['verified']): #want all friends
                        friends.append(user)
        return friends


    @staticmethod
    def find_interest_in_tweets(tweets):

        interest = []
        for status in tweets:
            tweet = status['text']
            interest.extend(TwitterUtil.find_insterest_in_text(tweet))

        return interest


    @staticmethod
    def find_insterest_in_text(text):
        nouns = []
        interest = []

        c_text = TwitterUtil.clean_tweet(text) #clean tweet
        if len(c_text) > 0 \
                and language(c_text)[0] == 'en':
                #and langid.classify(c_text)[0] == 'en'\

            c_text = utils.EnglishUtil.replace_slangs(c_text)
            c_text = utils.EnglishUtil.replace_i(c_text)

            positivity = positive(c_text) #if tweet is positive
            if not positivity:
               return interest


            nouns.extend(utils.NlpUtil.find_objects(c_text))

        for noun in nouns:
            eng = utils.EnglishUtil.only_english(noun)
            if len(eng) == 0:
                continue

            if len(eng) <= 4 or wordnet.synsets(singularize(eng)) or wordnet.synsets(pluralize(eng)) \
                    or 'some' in eng or 'any' in eng  or 'every' in eng or 'thing' in eng:
                continue

            interest.append( eng )

        return interest


    @staticmethod
    def trends():
        return Twitter().trends(cached=False)