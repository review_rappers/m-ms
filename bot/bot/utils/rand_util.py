import random


class RandUtil:
    # this will choose one and remove it
    @staticmethod
    def choose_and_remove(items):
        # pick an item index
        if items:
            index = random.randrange(len(items))
            return items.pop(index)
        # nothing left!
        return None

    @staticmethod
    def random_bool():
        return bool(random.getrandbits(1))