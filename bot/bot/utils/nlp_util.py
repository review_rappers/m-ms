from pattern.en import sentiment, polarity, subjectivity, positive, parsetree, parse, pprint, tag, suggest, wordnet
from pattern.en import Sentence, modality, mood, referenced, pluralize, singularize
from pattern.search import search, taxonomy, Classifier
from pattern.text import language
import re

class NlpUtil:
    @staticmethod
    def find_nouns(text):
        nouns = []
        for sentence in parse(text, relations=True, lemmata=True).split():
            for p in sentence:
                word = p[0]  #word
                tag = p[1] #tag eg JJ, NN
                if tag.startswith('NN'):
                    nouns.append(word)

        return nouns

    @staticmethod
    def find_objects(text):
        objects = []
        for sentence in parse(text, relations=True, lemmata=True).split():
            for p in sentence:
                word = p[0]  #word
                tag = p[1] #tag eg JJ, NN
                if tag.startswith('NN') and p[4].find('OBJ') != -1:
                    objects.append(word)

        return objects

    @staticmethod
    def find_is_a_noun(org_text):

        text = org_text
        is_a_pos = text.find(' is ')
        if is_a_pos == -1:
            return None

        # #find where it says " is "
        text = text[is_a_pos+4:]
        is_a_noun = ''

        sentence = parse(text, relations=True, lemmata=True).split()[0] #get first sentence only

        found_cat = False
        for p in sentence:
            word = p[0]  #word
            tag = p[1] #tag eg JJ, NN
            if tag == 'NN':
                is_a_noun += word + ' '
                found_cat = True
            elif found_cat:
                return is_a_noun.strip()

        return None

    @staticmethod
    def find_adjectives(text):
        adjectives = []
        if positive(text):
            for sentence in parse(text, relations=True, lemmata=True).split():
                for p in sentence:
                    word = p[0]  #word
                    if p[1] == 'JJ' and re.match("^[A-Za-z]*$", word):  # tag - Retrieve all adjectives.
                                adjectives.append(word)

        return adjectives
