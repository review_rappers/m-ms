import bitly_api
import sys

class UrlUtil:
    BITLY_ACCESS_TOKEN = "ce8c0237e0f2f05a2935b3215188bde8c04203b1"
    BITLY_USER = "bestproducts2015"
    bitly = bitly_api.Connection(access_token=BITLY_ACCESS_TOKEN)


    #usage UrlUtil.shorten('http://www.google.com')
    @staticmethod
    def shorten(url):
        return UrlUtil.bitly.shorten(url)['url']