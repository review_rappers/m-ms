import itertools

class ContainerUtil:
    @staticmethod
    def remove_from_dict(entries, the_dict):
        for key in entries:
            if key in the_dict:
                del the_dict[key]


    @staticmethod
    def chunks(l, n):
        """ Yield successive n-sized chunks from l.
        """
        for i in xrange(0, len(l), n):
            yield l[i:i+n]

    @staticmethod
    def flatten(l):
        #return [item for sublist in l for item in sublist]
        return list(itertools.chain.from_iterable(l))