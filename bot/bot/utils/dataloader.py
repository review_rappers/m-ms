import ast
import json

data_files = ["color_map", "bracketed_color_bigrams", "color_unigrams", "personality", "plural_color_bigrams",
              "unbracketed_color_bigrams"]


class DataLoader:
    def __init__(self, db):
        self.db

    def load_data(self):
        for data_file in data_files:
            file_path = "data/" + data_file + ".txt"
            table = self.db[data_file]
            with open(file_path, "r") as inp:

                lines = list(inp)
                columns = list(lines[0].lower().strip().split('\t'))
                lines = iter(lines)

                next(lines)
                for line in lines:
                    l = line
                    l = l.strip().split('\t')
                    row = dict()
                    for i in range(len(columns)):
                        row[columns[i]] = str(l[i])
                    row_json = ast.literal_eval(json.dumps(row, ensure_ascii=False))
                    print row_json
                    table.insert(row_json)

                inp.close()