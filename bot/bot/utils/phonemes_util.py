vowels = open("../data/cmudict.0.7a.phones.txt")
vowels = [v.strip() for v in vowels]         # ["AA\tvowel", ...]
vowels = [v.split("\t") for v in vowels]     # [["AA", "vowel"], ...]
vowels = dict(vowels)                        # {"AA": "vowel"}

phonemes = open("../data/cmudict.0.7a.txt").read()
phonemes = phonemes[phonemes.find("\nA "):].strip() # Skip vowels, comments and punctuation.
phonemes = phonemes.split("\n")            # ["A  AH0"]
phonemes = [p.split("  ") for p in phonemes] # [["A", "AH0"], ...]
phonemes = dict(phonemes)                    # {"A": "AH0"}
phonemes = dict((k, v.split(" ")) for k, v in phonemes.items()) # {"A": ["AH0"]}

# Split phoneme / stress
for w in phonemes:
    for i, p in enumerate(phonemes[w]):
        if p.endswith(("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")):
            phonemes[w][i] = (p[:-1], int(p[-1]) + 1) # "AH0" => ("AH", 1)
        else:
            phonemes[w][i] = (p, 0)                   # "B" => ("B", 0)

#print phonemes["PARROT"] # [('P', 0), ('EH', 2), ('R', 0), ('AH', 1), ('T', 0)]

def syllables(w):
    """ Returns the number of syllables
    """
    w = w.upper()
    i = 0
    if w in phonemes:
        for p in phonemes[w]:
            if vowels[p[0]] == "vowel":
                i += 1
        return i
    return None # unknown word

print syllables("encouraging") # 4
print syllables("parrot") # 2

def rhymes(w1, top=100):
    """ Returns a list of (word, number of rhyming ending phonemes)-tuples.
    """
    candidates = []
    w1 = w1.upper() # "parrot" => "PARROT"
    if w1 in phonemes:
        for w2 in phonemes:
            if w1 != w2:
                i = 0
                j = 0
                # More ending phonemes that match between w1 and w2 = better rhymes.
                # {"parrot": [('P', 0), ('EH', 2), ('R', 0), ('AH', 1), ('T', 0)]}
                # {"merit" : [('M', 0), ('EH', 2), ('R', 0), ('AH', 1), ('T', 0)]}
                for p1, p2 in zip(reversed(phonemes[w1]), reversed(phonemes[w2])):
                    if p1[0] != p2[0]:
                        break
                    if p1[1] == p2[1]: # phoneme stress matches
                        j += 1
                    i += 1
                if i != 0:
                    candidates.append((i, j, w2))
    candidates = sorted(candidates, reverse=True) # highest number of matches first
    candidates = [(w, i) for i, j, w in candidates]
    return candidates[:top]
    
for w, count in rhymes("elephant", top=100):
    print w, count
