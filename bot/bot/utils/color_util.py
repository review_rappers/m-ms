import math

from colormath.color_objects import sRGBColor

from colormath.color_conversions import convert_color
from colormath.color_objects import LabColor
from colormath.color_diff import delta_e_cie2000


class ColorUtil:
    @staticmethod
    def hex_distance(hex1, hex2):
        color1 = sRGBColor.new_from_rgb_hex(hex1)
        color1 = convert_color(color1, LabColor)
        color2 = sRGBColor.new_from_rgb_hex(hex2)
        color2 = convert_color(color2, LabColor)
        return ColorUtil.distance(color1, color2)

    @staticmethod
    def distance(color1, color2):
        return delta_e_cie2000(color1, color2)

    @staticmethod
    def blend(color1, color2, percentage1=50):
        percentage2 = 100 - percentage1;

        # ceil (all integers) regardless
        # calculate percentages (R1*p1%) + (R2*p2%)
        r = math.ceil(color1.rgb_r * percentage1 / 100) + math.ceil(color2.rgb_r * percentage2 / 100)

        # calculate percentages (G1*p1%) + (G2*p2%)
        g = math.ceil(color1.rgb_g * percentage1 / 100) + math.ceil(color2.rgb_g * percentage2 / 100)

        # calculate percentages (B1*p1%) + (B2*p2%)
        b = math.ceil(color1.rgb_b * percentage1 / 100) + math.ceil(color2.rgb_b * percentage2 / 100)

        # min 0
        # max 255
        r = 255 if r > 255 else r
        g = 255 if g > 255 else g
        b = 255 if b > 255 else b

        color = sRGBColor(r, g, b, True)
        return color

