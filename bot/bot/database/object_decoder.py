import services


def object_decoder(obj):
    if '__type__' in obj and obj['__type__'] == 'MetaphorEye':
        o = services.MetaphorEye()
        o.decode(obj['term'], obj['mashups'], obj['questions'])
        return o
    elif '__type__' in obj and obj['__type__'] == 'Rex':
        o = services.Rex()
        o.decode(obj['term'], obj['max_category'], obj['max_category_head'], obj['high_category'],
                 obj['high_category_head'], obj['categories'], obj['modifiers'], obj['categoryHeads'],
                 obj['allCategories'])
        return o
    elif '__type__' in obj and obj['__type__'] == 'Sardonicus':
        o = services.Sardonicus()
        o.decode(obj['term'], obj['factual'], obj['ironic'])
        return o

    return obj
