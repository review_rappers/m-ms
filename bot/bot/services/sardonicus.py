import json

from pattern.web import URL, DOM, plaintext

import database
from database import object_decoder


class Sardonicus:
    baseurl = "http://afflatus.ucd.ie/sardonicus/tree.jsp?adjective="

    def __init__(self, term=None):
        if not term:
            return

        self.__type__ = "Sardonicus"
        term = term.replace(" ", "_")

        self.term = term
        self.factual = []
        self.ironic = []

        db_obj = database.database.db.sardonicus.find_one({'term': term})
        if db_obj:
            obj = json.loads(db_obj['data'], object_hook=object_decoder.object_decoder)
            self.__dict__.update(obj.__dict__)
        else:
            self.parse()
            data = json.dumps(self, default=lambda o: o.__dict__)
            database.database.db.sardonicus.insert({'term': term, 'data': data})

    def parse(self):
        url = URL(Sardonicus.baseurl + self.term)
        dom = DOM(url.download(cached=True))
        factualTable = dom("table")[6]
        ironicTable = dom("table")[7]

        for item in factualTable("tr"):
            factual = plaintext(item.content).replace("_", " ").strip()
            self.factual.append(factual)

        for item in ironicTable("tr"):
            ironic = plaintext(item.content).replace("_", " ").strip()
            self.ironic.append(ironic)

    def decode(self, term, factual, ironic):
        self.term = term
        self.factual = factual
        self.ironic = ironic