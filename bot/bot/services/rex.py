from collections import defaultdict
import json

from pattern.web import URL, DOM, plaintext

import database
from database import object_decoder


class Rex:
    _threshold = 50

    def __init__(self, term=None):
        if not term:
            return

        self.__type__ = "Rex"
        self.max_category = None
        self.max_category_head = None
        self.high_category = defaultdict(list)
        self.high_category_head = []
        self.term = term
        self.categories = {}
        self.modifiers = {}
        self.categoryHeads = {}
        self.allCategories = {}

        db_obj = database.database.db.rex.find_one({'term': term})
        if db_obj:
            obj = json.loads(db_obj['data'], object_hook=object_decoder.object_decoder)
            self.__dict__.update(obj.__dict__)
        else:
            self.parse()
            data = json.dumps(self, default=lambda o: o.__dict__)
            database.database.db.rex.insert({'term': term, 'data': data})

    def parse(self):

        url = URL(
            "http://ngrams.ucd.ie/therex2/common-nouns/member.action?member=" + self.term + "&kw=" + self.term + "&needDisamb=true&xml=true")
        dom = DOM(url.download(cached=True))

        max_weight = -1
        for item in dom("CategoryHead"):
            weight = item.attrs.get("weight", "").strip()
            value = plaintext(item.content).strip()
            self.categoryHeads[value] = weight
            self.allCategories[value] = weight
            if weight > max_weight:
                max_weight = weight
                self.max_category_head = value

            if weight >= Rex._threshold:
                self.high_category_head.append(value)

        max_weight = -1
        for item in dom("Category"):
            weight = item.attrs.get("weight", "").strip()
            value = plaintext(item.content).strip()
            self.categories[value] = weight
            self.allCategories[value] = weight
            if value.endswith(self.max_category_head) and weight > max_weight:
                self.max_category = value[0: value.index(':')]
                max_weight = weight

            for cat_head in self.high_category_head:
                if value.endswith(cat_head):
                    self.high_category[cat_head].append(value[0: value.index(':')])

        for item in dom("Modifier"):
            weight = item.attrs.get("weight", "").strip()
            value = plaintext(item.content).strip()
            self.modifiers[value] = weight
            self.allCategories[value] = weight


    def decode(self, term, max_category, max_category_head, high_category, high_category_head, categories, modifiers,
               categoryHeads, allCategories):
        self.term = term
        self.max_category = max_category
        self.max_category_head = max_category_head
        self.high_category = high_category
        self.high_category_head = high_category_head
        self.categories = categories
        self.modifiers = modifiers
        self.categoryHeads = categoryHeads
        self.allCategories = allCategories

