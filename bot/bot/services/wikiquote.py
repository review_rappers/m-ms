import json
from pattern.web import URL, DOM, plaintext
import database
from database import object_decoder
from pattern.web import abs
import re
import json

class WikiQuote:
    baseurl = 'https://en.wikiquote.org/w/api.php?format=json&redirects&action=parse&prop=text&page='

    def __init__(self, name=None):
        if not name:
            return

        self.name = name
        self.quotes = []

        self.parse()

    def parse(self):
        search_name = self.name.replace(' ', '_')
        self.parse_url(WikiQuote.baseurl + search_name)


    def parse_url(self, url_link):
        url = URL(url_link)
        json_content = url.download(cached=True)
        data = json.loads(json_content)
        dom = DOM(data['parse']['text']['*'])
        content_dom = dom('#mw-content-text')
        if not content_dom:
            return

        full_content = plaintext(content_dom[0])
        for li in full_content.split("\n"):
            text = li

            #check if it quote
            if text.count(':') == 1:
                coln_pos = text.find(':')
                if coln_pos != -1:
                    name = text[:coln_pos]
                    if self.name.lower() not in name.lower():
                        continue
                    quote = text[coln_pos+1:]
                    print quote

            #check if it another link
            # link = li('a')
            # if link and self.name.lower in text.lower():
            #     link = link[0]
