import json
from pattern.web import URL, DOM, plaintext
import database
from database import object_decoder
from pattern.web import abs
import re

class Imdb:
    baseurl = 'http://www.imdb.com/'

    def __init__(self, name=None):
        if not name:
            return

        self.name = name
        self.character_url = ''
        self.quotes = []
        self.keywords = {}
        self.genres = {}

        self.get_character()
        self.get_quotes()
        self.get_keywords()
        self.get_genres()


    def get_character(self):
        character_url = 'http://www.imdb.com/find?q=' + self.name + '&s=ch&exact=true&ref_=fn_ch_ex'
        url = URL(character_url)
        dom = DOM(url.download(cached=True, throttle=2))

        results = dom('.result_text')
        if results:
            character_url = results[0]('a')[0].href
            character_url = abs(character_url, base=url.redirect or url.string)

        query_pos = character_url.find('?')
        if query_pos != -1:
            character_url = character_url[:query_pos]

        self.character_url = character_url



    def get_quotes(self):

        quotes = []
        if not self.character_url:
            return

        qoutes_url = self.character_url + '/quotes'
        url = URL(qoutes_url )
        dom = DOM(url.download(cached=True, throttle=2))

        main_content = dom('#tn15content')
        if not main_content:
            return

        main_content = main_content[0]
        content = main_content.content
        lines = content.split('<br />')
        for line in lines:
            text = plaintext(line)
            if text.count(':') != 1:
                continue
            coln_pos = text.find(':')
            if coln_pos != -1:
                name = text[:coln_pos]
                if self.name.lower() not in name.lower():
                    continue
                quote = text[coln_pos+1:]
                quotes.append(quote)

        self.quotes = quotes


    def get_keywords(self):
        keywords = {}
        if not self.character_url:
            return


        keywords_url = self.character_url + '/filmokey'
        url = URL(keywords_url)
        dom = DOM(url.download(cached=True, throttle=2))

        main_content = dom('#tn15content')
        if not main_content:
            return

        main_content = main_content[0]
        table = main_content('table')
        if table:
            table = table[0]
            for row in table('tr'):
                data = row('td')
                if not data:
                    continue

                index = data[0].content
                keyword = plaintext(data[1].content)
                keywords[keyword] = int(index)
                # print index + " " + keyword

        self.keywords = keywords

    def get_genres(self):
        #'filmogenre'
        keywords = {}
        if not self.character_url:
            return


        keywords_url = self.character_url + '/filmogenre'
        url = URL(keywords_url)
        dom = DOM(url.download(cached=True, throttle=2))

        main_content = dom('#tn15content')
        if not main_content:
            return

        main_content = main_content[0]
        table = main_content('table')
        if table and len(table) > 2:
            table = table[2]
            for row in table('tr'):
                data = row('td')
                if not data or len(data) != 2:
                    continue

                index = data[0].content
                keyword = plaintext(data[1].content)
                keywords[keyword] = int(index)
                # print index + " " + keyword

        self.genres = keywords

