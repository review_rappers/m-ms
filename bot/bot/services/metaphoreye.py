import json

from pattern.web import URL, DOM, plaintext

import database
from database import object_decoder


class MetaphorEye:
    baseurl = "http://ngrams.ucd.ie/metaphor-eye/SVLMashQuery?subj="

    def __init__(self, term=None):

        if not term:
            return

        term = term.replace(" ", "_")
        self.__type__ = "MetaphorEye"
        self.term = term
        self.mashups = []
        self.questions = []
        db_obj = database.database.db.metaphoreye.find_one({'term': term})
        if db_obj:
            obj = json.loads(db_obj['data'], object_hook=object_decoder.object_decoder)
            self.__dict__.update(obj.__dict__)
        else:
            self.parse()
            data = json.dumps(self, default=lambda o: o.__dict__)
            database.database.db.metaphoreye.insert({'term': term, 'data': data})

    def parse(self):
        url = URL(MetaphorEye.baseurl + self.term)
        dom = DOM(url.download(cached=True))
        mashupsTable = dom("table")[4]
        questionsTable = dom("table")[5]

        for item in mashupsTable("tr"):
            span = item("span")
            if span:
                text = plaintext(span[0].content).strip()
                pos = text.find(self.term) + len(self.term) + 1
                self.mashups.append(text[pos:].strip())

        for item in questionsTable("tr"):
            span = item("span")
            if span:
                text = plaintext(span[0].content).strip()
                pos = text.find(self.term) + len(self.term) + 1
                self.questions.append(text[pos:].strip())

    def decode(self, term, mashups, questions):
        self.term = term
        self.mashups = mashups
        self.questions = questions