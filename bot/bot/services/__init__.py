__all__ = ['Sardonicus', 'Rex', 'MetaphorEye', 'Imdb', 'WikiQuote']

from sardonicus import Sardonicus
from rex import Rex
from metaphoreye import MetaphorEye
from imdb import Imdb
from wikiquote import WikiQuote