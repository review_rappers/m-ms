import json
from tweepy.streaming import StreamListener
import tweepy
import tweets
import bot

class BotHandler(StreamListener):
    def on_data(self, data):
        stream = json.loads(data)

        # if not my account
        if bot.BOT_ID != stream['user']['id']:
            tweet = tweets.BattleTweet()
            response = tweet.generate()
            if response:
                #bot.tweet_message(response, stream, True, False)
                bot.tweet_message(response, None, True, False)


    def on_error(self, status):
        print status
