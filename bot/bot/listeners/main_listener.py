import json
import traceback

from tweepy.streaming import StreamListener
import tweepy

import bot
import listeners

class MainHandler(StreamListener):
    def on_data(self, data):
        try:
            stream = json.loads(data)
            if bot.BOT_ID == stream['user']['id']:  # if my bot tweeted
                return

            listeners.bot_listener.BotHandler().on_data(data)
        except Exception, err:
            print traceback.format_exc()
            #or
            #print sys.exc_info()[0]
        except:
            pass


class MainListener():
    def __init__(self, auth):
        self.l = MainHandler()
        self.auth = auth

    def listen(self):
        stream = tweepy.Stream(self.auth, self.l)
        stream.filter(follow=[str(bot.BOT_ID)], track=[])