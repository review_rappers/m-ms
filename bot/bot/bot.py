import __builtin__

import tweepy
from uclassify import uclassify
import tweets
import listeners
import requests

# twitter keys
CONSUMER_KEY = "Ns4z2HpvtFetytDPEZAbTQlLj"
CONSUMER_KEY_SECRET = "8dSE2LCrubaB97Hi7DQFbQu2PhwPdfTp63kbVlBJN12MjCt6L7"
ACCESS_TOKEN = "2978321692-14PVdQ8X8fOVWrPYn4GExVQVUEIJzrw6SkAkhSD"
ACCESS_TOKEN_SECRET = "9Xi0WbKirRQT8Gjfd2ADkhKqb7QWlU0Z3rVD5E2uU6T47"
#uclassify
UCLASSIFY_READ = ""
UCLASSIFY_WRITE = ""
#google key
GOOGLE_KEY = ""

BOT_ID = 2958281273


twitter = None
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_KEY_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
twitter = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

uClassify = uclassify()
uClassify.setWriteApiKey(UCLASSIFY_WRITE)
uClassify.setReadApiKey(UCLASSIFY_READ)

def tweet_message(message, in_reply_to=None, retweet=False, display=False):
    # fix grammar of the sentence
    # tool = language_check.LanguageTool('en-US')
    # matches = tool.check(message)
    # message = language_check.correct(message, matches)

    #then tweet it
    if retweet and in_reply_to:
        in_reply_to_id = in_reply_to['id']
        screen_name = in_reply_to['user']['screen_name']
        message = "@" + screen_name + " " + message

        if display:
            message = "." + message

        if len(message) <= 140:
            twitter.update_status(message, in_reply_to_id)
    elif len(message) <= 140:
        twitter.update_status(message)


class Bot:
    def __init__(self):
        self.twitter = twitter
        self.auth = auth

    def test(self):
        print "Test"
        #test calling advertise http://tweeterid.com/
        #print utils.NocUtil.character_has_in_column('Category', 'Actor')
        tweets.BattleTweet().generate()

    def start(self):
        listener = listeners.MainListener(self.auth)
        listener.listen()

if __name__ == "__main__":
    from classifiers import GenderByName
    __builtin__.gbn = GenderByName.load("data/gender-by-name.svm")

    bot = Bot()
    bot.test()
    #bot.start()


