try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My Bot',
    'author': 'Khalid Alnajjar',
    'url': 'http://www.twitter.com/bot',
    'download_url': 'Where to download it.',
    'author_email': 'khldalnajjar@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['bot'],
    'scripts': [],
    'name': 'bot'
}

setup(**config,
      requires=['nose', 'colormath', 'pymongo', 'tweepy', 'numpy', '3to2', 'google-api-python-client', 'crypto', 'pyopenssl', 'bitly_api', 'python-amazon-simple-product-api', 'language_check', 'httplib2', 'uclassify', 'langid'])