import metre_rhyme as mr
import random
import re 
from imdb import *
import os
import nltk
from pattern.db import Datasheet
from pattern.en import sentiment
from nltk.tokenize import RegexpTokenizer
from RhymeBrain import *
from similarity import *
import math

class Character:

	def __init__(self, me):
		self.tokenizer = RegexpTokenizer(r'\w+')
		self.me = me
		self.name = me["Character"]
		self.initials = "".join(re.findall("[A-Z]", self.name))
		self.initialize_quotes()
		self.initalize_templates()
		self.tweeted_stuff = list()
		# If you want to destroy the system, then please
		# sort the lists above in a random place
		#
		# I.E. DO NOT SORT THE LISTS!!!!111
		#
		self.character_specific = self.quotes + self.templates
		self.character_specific_list = self.quotes_list + self.templates_list
		self.character_specific_num = len(self.character_specific_list)
		self.tweeted_stuff = list()
		
	def optimize_two_lines(self, matching_score, similarity, pol1, pol2, subj1, subj2):
		alpha = 0.6
		beta = 0.2
		gamma = 0.2
		delta = 0.1
		epsilon = 0.1
		if matching_score < -10:
			matching_score = -10
		matching_score += 10
		matching_score /= 13
		
		diff_pol = math.fabs(pol1-pol2)/2
		diff_sub = math.fabs(subj1-subj2)/2
		
		score = alpha*matching_score + beta*similarity + delta*diff_pol + epsilon*diff_sub
		return score

	
	def calculate_pairs(self, num_needed=20):
		rhymer = Rhyme()
		#use_rhymebrain = False
		self.pairs = list()
		i = 0
		while len(self.pairs) < 15:
			print len(self.pairs)
			i = random.randint(0,self.num_reviews-1)
			r = self.reviews_list[i]
			if len(r) > 10:
				try:
					rhymes = rhymer.rhyme(r, self.character_specific_list)
					if rhymes:
						for rr in rhymes:
							j = self.character_specific_list.index(rr)
							if len(rr) + len(r) + 4 <= 140:
								sim = self.similarity(r,rr)
								opp_adj = 0
								#rhyme = self.does_rhyme("","")
								sentiment1 = sentiment(rr)
								sentiment2 = sentiment(r)
							
								# returns score, fixed1, fixed2
								fixed_metre = mr.matching_score_nsyllables_fix(r, rr, self.user_reviews[i][2], self.character_specific[j][2])
								self.reviews_list[i] = fixed_metre[1]
								self.user_reviews[i][0] = fixed_metre[1]
								self.character_specific_list[j] = fixed_metre[2]
								self.character_specific[j][0] = fixed_metre[2]
								opt_score = self.optimize_two_lines(fixed_metre[0], sim, sentiment1[0], sentiment2[0], sentiment1[1], sentiment2[1])
								self.pairs.append([ j,i, opt_score, sim, True, sentiment1[0], sentiment1[1], sentiment2[0], sentiment2[1]])
								break
				except:
					j = random.randint(0,self.character_specific_num-1)
					rr = self.character_specific[j][0]
					if len(rr) + len(r) + 4 <= 140:
						sim = self.similarity(r,rr)
						opp_adj = 0
						#rhyme = self.does_rhyme("","")
						sentiment1 = sentiment(rr)
						sentiment2 = sentiment(r)
					
						fixed_metre = mr.matching_score_nsyllables_fix(r, rr, self.user_reviews[i][2], self.character_specific[j][2])
						self.reviews_list[i] = fixed_metre[1]
						self.user_reviews[i][0] = fixed_metre[1]
						self.character_specific_list[j] = fixed_metre[2]
						self.character_specific[j][0] = fixed_metre[2]
						opt_score = self.optimize_two_lines(fixed_metre[0], sim, sentiment1[0], sentiment2[0], sentiment1[1], sentiment2[1])
						self.pairs.append([ j,i, opt_score, sim, True, sentiment1[0], sentiment1[1], sentiment2[0], sentiment2[1]])
		self.pairs = sorted(self.pairs, key=lambda x: x[2], reverse=True)
		self.pairs = self.pairs[:num_needed]
	
	def similarity(self, s1, s2):
		return sentence_similarity(s1,s2)
		
	def does_rhyme(self, s1, s2):
		return False
		
	def rap(self, lines_needed):
		self.calculate_pairs(num_needed=lines_needed)
		
	def is_splittable_category(self, cat):
		if "," in self.me[cat]:
			vehicles = map(lambda x: x.strip(), self.me[cat].split(","))
			return vehicles
		else:
			return [self.me[cat]]
		
	def initalize_templates(self):
		fname = "templates_initialized/" + self.name + ".csv"
		self.templates = list()
		if not os.path.exists(fname):
			f = open("eminem_templates.txt")
			dat = f.read().split("\n")
			f.close()
			da = Datasheet(fname)
			for line in dat:
				if "<ADDRESS>" in line:
					changed = re.sub("<ADDRESS>", self.me["Address 1"], line)
					da.append(self.create_quadruple(changed))	
				elif "<VEHICLE>" in line:
					vehicles = self.is_splittable_category("Vehicle of Choice")
					for v in vehicles:
						changed = re.sub("<VEHICLE>", v, line)
						da.append(self.create_quadruple(changed))	
				elif "<CLOTHING>" in line:
					vehicles = self.is_splittable_category("Seen Wearing")
					for v in vehicles:
						changed = re.sub("<CLOTHING>", v, line)
						da.append(self.create_quadruple(changed))	
				elif "<ACTIVITY>" in line:
					vehicles = self.is_splittable_category("Typical Activity")
					for v in vehicles:
						changed = re.sub("<ACTIVITY>", v, line)
						da.append(self.create_quadruple(changed))	
				elif "<WEAPON>" in line:
					vehicles = self.is_splittable_category("Weapon of Choice")
					for v in vehicles:
						changed = re.sub("<WEAPON>", v, line)
						da.append(self.create_quadruple(changed) + ["template"])
				self.templates.append(da[-1])
				self.templates += map(lambda x: self.create_quadruple(x) + ["template"], replace_synynoms(da[-1][0])[1:])
			da.save(fname)
		else:
			da = Datasheet.load(fname)
			for line in da:
				self.templates.append([line[0], int(line[1]), int(line[2]), line[3], int(line[4])])
		self.templates_list = map(lambda x: x[0], self.templates)
		self.num_templates = len(self.templates)
		
	def set_reviews(self, rev):
		self.reviews = rev
		self.user_reviews = []
		for r in self.reviews:
			self.user_reviews += filter(lambda x: len(x[0]) < 60, r.review_splitted)
			for split_rev in r.review_splitted:
				self.user_reviews += map(lambda x: self.create_quadruple(x) + ["review"], replace_synynoms(split_rev[0])[1:])
		self.user_reviews = map(lambda x: x + ["review"], self.user_reviews)
		self.num_reviews = len(self.user_reviews)
		self.reviews_list = map(lambda x: x[0], self.user_reviews)
		
	def create_quadruple(self, q):
		s = q
		ls = len(s)
		nss = mr.n_syllables(s)
		q = q.replace("'", "")
		tk = self.tokenizer.tokenize(q)
		ltk = len(tk)
		tk = " ".join(tk)
		return [s, ls, nss, tk, ltk]
			
	def initialize_quotes(self):
		fname = "quotes/" + self.name + ".csv"
		if not os.path.exists(fname):
			quotes = []
			try:
				imdb = Imdb(self.name)
				for x in imdb.quotes:
					quotes += nltk.sent_tokenize(re.sub("\[.+?\]", "",x))
					quotes = map(lambda x: x.strip(), quotes)
			except:
				pass
			da = Datasheet(fname)
			qq = list()
			for q in quotes:
				if len(q) > 3 and len(q) < 60:
					da.append(self.create_quadruple(q) + ["quote"])
					synquotes = replace_synynoms(da[-1][3])[1:]
					for s in synquotes:
						da.append(self.create_quadruple(s) + ["quote"])
			da.save(fname)
			self.quotes = da
		else:
			da = Datasheet.load(fname)
			self.quotes = list()
			for line in da:
				self.quotes.append([line[0], int(line[1]), int(line[2]), line[3], int(line[4])])
		self.quotes_list = map(lambda x: x[0], self.quotes)
		self.num_quotes = len(self.quotes)
		
	def optimize_four_lines(self, line1, line2, line3, line4):
		r1 = line1 + " " + line2
		r2 = line1 + " " + line2
		sim = self.similarity(r1,r2)
		opp_adj = opposite_adjectives(r1,r2)
		metre = mr.matching_score(r1,r2)
		score = 0.7*sim + 0.3*opp_adj
		return score
		
	def get_next_line(self, previous_lines=[]):
		if not previous_lines:
			choice = random.choice(self.pairs)
			line1 = self.character_specific[choice[0]][0]
			line2 = self.user_reviews[choice[1]][0]
		else:
			candidates = list()
			for p in self.pairs:
				line1 = self.character_specific[p[0]][0]
				line2 = self.user_reviews[p[1]][0]
				score = self.optimize_four_lines(line1, line2, previous_lines[-1][0], previous_lines[-1][1])
				candidates.append([p,score])
			candidates = sorted(candidates, key=lambda x: x[1], reverse=True)
			choice = candidates[0][0]
			line1 = self.character_specific[choice[0]][0]
			line2 = self.user_reviews[choice[1]][0]		
			
		line1 = self.character_specific[choice[0]][0]
		line2 = self.user_reviews[choice[1]][0]

		# row = post_id, sentence_1, sentence_2, int_matching_score, float_similarity, bin_rhymes, polarity_first, subjectivity_first, polarity_second, subjectivity_second, retweets, favorites
		generation_data = [line1, line2] + choice[2:]
		self.pairs.remove(choice)
		return [line1, line2, generation_data]
