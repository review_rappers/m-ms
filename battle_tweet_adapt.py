from Reviews import *
from CharacterAdapt import *
import similarity as s
import noc
import random
import glob
import tweepy
from Adaption import *
# twitter keys
CONSUMER_KEY = "Ns4z2HpvtFetytDPEZAbTQlLj"
CONSUMER_KEY_SECRET = "8dSE2LCrubaB97Hi7DQFbQu2PhwPdfTp63kbVlBJN12MjCt6L7"
ACCESS_TOKEN = "2978321692-14PVdQ8X8fOVWrPYn4GExVQVUEIJzrw6SkAkhSD"
ACCESS_TOKEN_SECRET = "9Xi0WbKirRQT8Gjfd2ADkhKqb7QWlU0Z3rVD5E2uU6T47"

twitter = None
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_KEY_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
twitter = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

import time

# Choose opponents

a = list(noc.assoc(noc.xlsx("NOC_List/NOC_List.xlsx")))

#char_a = Character(a[7])
#char_b = Character(a[8])

adapt = Adaption("data/adaption_data.csv")

while True:

	opponent = []
	while not opponent:
		n = random.randint(0,len(a)-1)
		char_a = Character(a[n])
		if char_a.num_quotes > 50:
			opponent = s.find_opponent(char_a.me)

			if opponent:
				char_b = Character(opponent[0][0])
				if char_b.num_quotes < 50:
					opponent = None

	print char_a.name,"vs",char_b.name

	### Assign sides
	r = random.random()

	a_side = -1

	if r > 0.5:
		a_side = 1

	movies = glob.glob("reviews/*.csv")
	random.shuffle(movies)
	movie = movies[0].replace("\\",'/').split("/")[1].split("_")[0]

	r = Reviews(movie)

	pos = r.get_positive_reviews()
	neg = r.get_negative_reviews()

	needed_tweets = 5

	if a_side == -1:
		char_a.set_reviews(neg)
		char_b.set_reviews(pos)
	else:
		char_a.set_reviews(pos)
		char_b.set_reviews(neg)

	char_a.rap(needed_tweets)	
	char_b.rap(needed_tweets)

	in_reply_to = False	
	tweets = [ [ "Legendary MCs " + char_a.name + " (MC " + char_a.initials + ") vs " + char_b.name + " (MC " + char_b.initials + ") reviewing " + movie + " #codecampcc", [] ] ]

	previous_lines = list()
	for i in range(needed_tweets):
		nexta = char_a.get_next_line(adapt, previous_lines)
		tweets.append([ char_a.initials + ":\n" + "\n".join(nexta[:2]), nexta[2:] ])
		previous_lines.append(nexta[:2])
		nextb = char_b.get_next_line(adapt, previous_lines)
		previous_lines.append(nextb[:2])
		tweets.append([ char_b.initials + ":\n" + "\n".join(nextb[:2]), nextb[2:] ])
	
	for t in tweets:
		print t[0]
		if in_reply_to:
			status = twitter.update_status(t[0], in_reply_to)
		else:
			status = twitter.update_status(t[0])

		in_reply_to = status['id']

		if t[1]:
			sl = [status['id']]
			for element in t[1][0]:
				sl.append(element)
			adapt.add_row(sl)
		time.sleep(121)
	twitter.update_status("All for now bros. Who you think was da winner?", in_reply_to)
	time.sleep(4000)
	adapt.update_all_rows()
