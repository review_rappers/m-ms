import noc
import random
from pattern.en import wordnet, NOUN, VERB, ADJECTIVE, ADVERB, tag, parsetree

def parse(a):
	a = a.split(",")
	r = list()
	for s in a:
		s = s.strip()
		if len(s) > 2:
			r.append(s)
	return set(r)
	
def create_opposite_character(a):
	a_pos = list(parse(a["Positive Talking Points"]))
	a_neg = list(parse(a["Negative Talking Points"]))
	
	traits = set()
	
	for trait in a_pos+a_neg:
		synset = wordnet.synsets(trait, pos=ADJECTIVE)
		if len(synset) > 0:
			synset = synset[0]
			ant = synset.antonym
			if ant is not None:
				traits |= set(ant.synonyms)
	return traits


def jaccard(a,b):	
	if len(a|b) > 0:
		return float(len(a&b))/len(a|b) 
	else:
		return 0

def weighted_score(a,b):
	a_genre = parse(a["Genres"])
	a_category = parse(a["Category"])
	a_domain = parse(a["Domains"])
	a_typical_act = parse(a["Typical Activity"])
	a_pos = parse(a["Positive Talking Points"])
	a_neg = parse(a["Negative Talking Points"])
	
	b_genre = parse(b["Genres"])
	b_category = parse(b["Category"])
	b_domain = parse(b["Domains"])
	b_typical_act = parse(b["Typical Activity"])
	b_pos = parse(b["Positive Talking Points"])
	b_neg = parse(b["Negative Talking Points"])
	
def find_opponent(character_a):	
	a = list(noc.assoc(noc.xlsx("NOC_List/NOC_List.xlsx")))
	traits_a = set(character_a["Positive Talking Points"])|set(character_a["Negative Talking Points"])
	opp = set(create_opposite_character(character_a))

	l = list()
	for b in a:
		traits_b = set(parse(b["Positive Talking Points"]))|set(parse(b["Negative Talking Points"]))
		scr = jaccard(opp, traits_b)
		if scr > 0:
			l.append([b, scr])
	
	l = sorted(l, key=lambda x: x[1], reverse=True)
	return l

def sentence_similarity(text_1, text_2):
	sim_sum = 0.0
	n_pairs = 0.0
	for w1, pos1 in tag(text_1):
		if pos1 == "NN":
			s1 = wordnet.synsets(w1)
			if s1:
				s1 = s1[0]
			else:
				continue
			for w2, pos2 in tag(text_2):
				if pos2 == "NN":
					s2 = wordnet.synsets(w2)
					if s2:
						s2 = s2[0]
					else:
						continue
					sim_sum += wordnet.similarity(s1, s2)
					n_pairs += 1
	if n_pairs:
		return sim_sum / n_pairs
	return 0

def opposite_adjectives_simple(text_1, text_2):
	for w1, pos1 in tag(text_1):
		if pos1 == "JJ":
			synsets_1 = wordnet.synsets(w1, pos=ADJECTIVE)
			if synsets_1:
				s1 = synsets_1[0]
			else:
				continue
			ant = s1.antonym
			for w2, pos2 in tag(text_2):
				if pos2 == "JJ":
					synsets_2 = wordnet.synsets(w2, pos=ADJECTIVE)
					if synsets_2:
						s2 = synsets_2[0]
					else:
						continue
					if intersection(ant, s2.synonyms):
						return True
	return False
					
def opposite_adjectives(text_1, text_2):
	
	if "not" not in text_1.split() and "not" not in text_2.split():
		return opposite_adjectives_simple(text_1, text_2)

	for sent1 in parsetree(text_1):
#		print sent1
		for chunk1 in sent1.chunks:
			for w1 in chunk1.words:
				if w1.tag == "JJ":
#					print w1
					sets_1 = wordnet.synsets(w1.string, pos=ADJECTIVE);
					if sets_1:
						s1 = sets_1[0]
					else:
						continue
					if "not" in chunk1:
						to_comp_1 = s1.synonyms
					else:
						to_comp_1 = s1.antonym
					for sent2 in parsetree(text_2):
#						print sent2
						for chunk2 in sent2.chunks:
							for w2 in chunk2.words:
								if w2.tag == "JJ":
#									print w2
									sets_2 = wordnet.synsets(w2.string, pos=ADJECTIVE);
									if sets_2:
										s2 = sets_2[0]
									else:
										continue
									if "not" in chunk2:
										to_comp_2 = s2.antonym
									else:
										to_comp_2 = s2.synonyms
									if intersection(to_comp_1, to_comp_2):
										return True
	return False

def intersection(a, b):
	if not a or not b:
		return []
	return list(set(a) & set(b))

if __name__ == "__main__":
	s1 = "Hello, is it me you're looking for?"
	s2 = "This sentence is a test"
	s3 = "This string is used for demo purposes"
	s4 = "He is a very bad guy"
	s5 = "It is good when it is not raining"
	s6 = "It is not good, ok?"
	s7 = "A bad movie"
	print sentence_similarity(s1, s2)
	print sentence_similarity(s3, s2)

	print opposite_adjectives(s4, s5)
	print opposite_adjectives(s5, s6)
	print opposite_adjectives(s4, s7)
#	print wordnet.synsets("good", pos=ADJECTIVE)
#	print wordnet.synsets("bad", pos=ADJECTIVE)